package com.level.shard.controller;

import com.level.shard.entity.OrderInfo;
import com.level.shard.service.OrderInfoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.util.List;

@RestController
@RequestMapping("/test")
public class OrderInfoController {

    @Autowired
    private OrderInfoService orderInfoService;

    @PostMapping("/add")
    public String add() throws ParseException {
        return orderInfoService.add();
    }


    @RequestMapping("/get")
    @ResponseBody
    public List<OrderInfo> get() {
        return orderInfoService.get();
    }
}