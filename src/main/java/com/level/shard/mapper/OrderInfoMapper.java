package com.level.shard.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.level.shard.entity.OrderInfo;
import org.springframework.stereotype.Component;

@Component
public interface OrderInfoMapper extends BaseMapper<OrderInfo> {

}