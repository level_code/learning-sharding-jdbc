package com.level.shard.service;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.level.shard.entity.OrderInfo;
import com.level.shard.mapper.OrderInfoMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
@Slf4j
@Service
public class OrderInfoService {

    @Autowired
    private OrderInfoMapper orderInfoMapper;

    public String add() throws ParseException {
        for (int i = 0; i < 100; i++) {
            OrderInfo orderInfo = new OrderInfo();
            orderInfo.setId("R_"+UUID.randomUUID().toString());
            orderInfo.setUserId(i);
            orderInfo.setCreateDate(LocalDateTime.now());
            orderInfoMapper.insert(orderInfo);
        }
        for (int i = 0; i < 100; i++) {
            OrderInfo orderInfo = new OrderInfo();
            orderInfo.setId("O_"+UUID.randomUUID().toString());
            orderInfo.setUserId(i);
            orderInfo.setCreateDate(LocalDateTime.now());
            orderInfoMapper.insert(orderInfo);
        }
        return "0";
    }

    public List<OrderInfo> get() {
        List<OrderInfo> orderInfos = orderInfoMapper.selectList(new QueryWrapper<>());
        return orderInfos;
    }

}
