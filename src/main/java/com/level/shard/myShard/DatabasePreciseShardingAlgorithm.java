package com.level.shard.myShard;

import org.apache.shardingsphere.api.sharding.standard.PreciseShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingValue;

import java.util.Collection;

public class DatabasePreciseShardingAlgorithm implements PreciseShardingAlgorithm<Integer> {
    @Override
    public String doSharding(Collection<String> collection, PreciseShardingValue<Integer> preciseShardingValue) {
        // 获取分片键的值
        Integer shardingValue = preciseShardingValue.getValue();
        // 获取逻辑
        String logicTableName = preciseShardingValue.getLogicTableName();
        System.out.println("分片键的值:{},逻辑表:{}"+ shardingValue+"_"+logicTableName);
        int sourceTarget = shardingValue % 2;
        // 遍历数据源
        for (String databaseSource : collection) {
            // 判断数据源是否存在
            if (databaseSource.endsWith(sourceTarget + "")) {
                return databaseSource;
            }
        }
        // 不存在则抛出异常
        throw new UnsupportedOperationException();
    }
}
